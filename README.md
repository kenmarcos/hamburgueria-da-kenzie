# Kenzie Burguer

Link do site: https://hamburgueria-da-kenzie-gamma.vercel.app/

## Sobre o Projeto

Esse projeto foi realizado como parte integrante das atividades de ensino na Kenzie Academy Brasil.
Seu principal objetivo era criar uma aplicação que simulasse o mecanismo básico de um carrinho de compras.

## Conhecimentos aplicados

Fundamentos básicos de React JS: componentização e manipulação de props e state.
