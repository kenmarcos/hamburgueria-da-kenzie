import "./App.css";
import { useState } from "react";
import Header from "./components/Header";
import Details from "./components/Details";
import MenuContainer from "./components/MenuContainer";
import Info from "./components/Info";
import ShowSale from "./components/ShowSale";
import Prepare from "./components/Prepare";

function App() {
  const [products] = useState([
    { id: 1, name: "Hamburguer", category: "Sanduíches", price: 7.99 },
    { id: 2, name: "X-Burguer", category: "Sanduíches", price: 8.99 },
    { id: 3, name: "X-Salada", category: "Sanduíches", price: 10.99 },
    { id: 4, name: "Big Kenzie", category: "Sanduíches", price: 16.99 },
    { id: 5, name: "Guaraná", category: "Bebidas", price: 4.99 },
    { id: 6, name: "Coca", category: "Bebidas", price: 4.99 },
    { id: 7, name: "Fanta", category: "Bebidas", price: 4.99 },
  ]);

  const [filteredProducts, setFilteredProducts] = useState([]);

  const [currentSale, setCurrentSale] = useState([]);

  const [cartTotal, setCartTotal] = useState(0);

  const [isBuy, setIsBuy] = useState(false);

  const showProducts = (productToSearch) => {
    setFilteredProducts(
      products.filter(
        (item) =>
          item.name.toLocaleLowerCase() === productToSearch.toLocaleLowerCase()
      )
    );
  };

  const handleClick = (productId) => {
    setCurrentSale([
      ...currentSale,
      products.find((product) => product.id === productId),
    ]);
  };

  const showPreparation = () => {
    setIsBuy(true);
  };

  const backProducts = () => {
    setFilteredProducts([]);
  };

  return (
    <div className="App">
      <Header showProducts={showProducts} />
      <Details />
      <div className="menuContainer">
        <MenuContainer
          products={products}
          handleClick={handleClick}
          filteredProducts={filteredProducts}
          backProducts={backProducts}
        />
      </div>
      <ShowSale
        currentSale={currentSale}
        cartTotal={cartTotal}
        setCartTotal={setCartTotal}
        showPreparation={showPreparation}
      />
      <Info />
      <Prepare isBuy={isBuy} />
    </div>
  );
}

export default App;
