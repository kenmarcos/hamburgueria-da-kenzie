import "./style.css";
import prepare from "./prepare.png";

const Prepare = ({ isBuy }) => {
  return (
    <>
      {isBuy === true ? (
        <div className="preparationContainer">
          <h2 className="preparationTitle">
            Seu pedido já está sendo preparado.
          </h2>
          <figure>
            <img alt="foods" src={prepare} />
          </figure>
          <h2 className="preparationTitle">Aguarde!</h2>
        </div>
      ) : null}
    </>
  );
};

export default Prepare;
