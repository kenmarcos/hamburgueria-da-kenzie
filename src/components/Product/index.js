import "./style.css";

const Product = ({ products, handleClick, filteredProducts, backProducts }) => {
  return (
    <div className="containerProducts">
      <h2 className="titleContainer">&lt;Cardápio&gt;</h2>

      {filteredProducts[0] === undefined ? (
        products.map((product, index) => (
          <div key={index} className="cardProduct">
            <h2 className="titleProduct">{product.name}</h2>
            <p className="categoryProduct">Categoria: {product.category}</p>
            <p className="priceProduct">Preço: R$ {product.price}</p>
            <div className="divAddBtn">
              <button
                className="addBtn"
                onClick={() => handleClick(product.id)}
              >
                Adicionar
              </button>
            </div>
          </div>
        ))
      ) : (
        <div className="cardProduct">
          <h2 className="titleProduct">{filteredProducts[0].name}</h2>
          <p className="categoryProduct">
            Categoria: {filteredProducts[0].category}
          </p>
          <p className="priceProduct">Preço: R$ {filteredProducts[0].price}</p>
          <div className="divAddBtn">
            <button
              className="addBtn"
              onClick={() => handleClick(filteredProducts[0].id)}
            >
              Adicionar
            </button>
          </div>
        </div>
      )}
      {filteredProducts[0] !== undefined && (
        <button className="backBtn" onClick={backProducts}>
          Voltar
        </button>
      )}
    </div>
  );
};

export default Product;
